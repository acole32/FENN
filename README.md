# FENN - Fast Explicit Neutrino Networks

## A High-Performance Neutrino Transport Simulation in Astrophysical Environments

FENN (Fast Explicit Neutrino Networks) is a state-of-the-art tool that simulates neutrino transport in computationally challenging astrophysical scenarios such as core collapse supernova explosions and neutron star mergers. It provides a unique solution to handling the computational stiffness typically encountered in these settings, by utilizing explicit methods that are not only easier to implement but also consume less memory than traditional implicit methods. Despite the perceived impracticality of explicit methods for stiff systems, FENN showcases an approach that yields an acceptable controlled error, making large-scale and realistic kinetic network simulations more efficient and viable.

## 🚀 Getting Started

### Table of Contents

- [Prerequisites](#-prerequisites)
- [Installation](#-installation)
- [Inputs](#-inputs)
- [Running the Simulation](#-running-the-simulation)
- [Plotting and Graphics Package](#-plotting-and-graphics-package)
- [Outputs](#-outputs)
- [Restarting the Simulation](#-restarting-the-simulation)
- [Built With](#built-with)
- [Authors](#authors)
- [Acknowledgments](#acknowledgments)
- [Questions](#questions)
- [License](#license)

### 🔧 Prerequisites

To run FENN, you need:

* C++11 or higher
* C++ standard libraries for input/output, math, strings, and vectors

### 📥 Installation

1. Download or clone this repository
2. Compile the code with a C++ compiler
3. Run the executable file

### 📝 Inputs

FENN requires several computational parameters to be defined at the start of the main function, such as the model, number of energy groups, final time, initial time, write time, and tolerances for different calculations.

### 🏃‍♀️ Running the simulation

After compiling the code, run the resulting executable file. The simulation will then execute based on the parameters you defined.

To compile the code, use the command:

```
g++ Networks.cpp -o FENN
```

To run the simulation: 
```
./FENN
```

The results will be available as plotfiles in the 'Output' folder.

## 🖌️ Plotting and Graphics Package

FENN comes equipped with a powerful Plotting and Graphics Package, allowing for effective data visualization and error analysis.

### Standard Plotting Package

The Standard Plotting Package provides a user-friendly interface for plotting data in the form of timestep vs. time and population values. Customize plot file numbers and titles to fit your needs.

### Comparison Packages for Error Analysis

The Comparison Packages enable a detailed comparison between Explicit Methods and standard Implicit Methods, providing valuable insights into the accuracy and efficiency of different numerical methods.

For detailed usage instructions, please refer to our documentation.

### 📋 Outputs

FENN generates log files and plot files, which contain essential simulation parameters and data respectively. Define the output directory and file names within the code.

### 🔁 Restarting the simulation

FENN allows for simulations to be restarted from a previous output file. To do this, specify the restart file number in the main function and re-run the simulation.

## Built With

* C++

## Authors

* Raghav Chari, Undergraduate Student | University of Tennessee, Knoxville 
* Adam Cole, Graduate Student | University of Tennessee, Knoxville 

## Acknowledgments

Our project builds upon developmental MATLAB calculations and NES Rates from Dr. Eirik Endeve at the Oak Ridge National Labs.

## Questions 

Feel free to reach out to Raghav Chari: Rchari1@vols.utk.edu


