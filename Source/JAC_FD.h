#include <vector>
#include <cmath>
#include <algorithm>
#include <numeric>



std::vector<std::vector<double> > JAC_FD(std::vector<double> N, std::vector<std::vector<double> > R_In, std::vector<std::vector<double> > R_Out, int N_g) {
    std::vector<std::vector<double> > JAC(N_g, std::vector<double> (N_g, 0));
    for (int i = 0; i < N_g; i++) {
        for (int j = 0; j < N_g; j++) {
            if (i != j) {
                JAC[i][j] = (1 - N[i]) * R_In[i][j] - N[i] * R_Out[i][j];
            }
        }
    }
    for (int i = 0; i < N_g; i++) {
        std::vector<double> ones_N_g(N_g, 1);
        for (int j = 0; j < N_g; j++) {
            ones_N_g[j] -= N[j];
        }
        double sum_R_In = 0.0, sum_R_Out = 0.0;
        for (int j = 0; j < N_g; j++) {
            sum_R_In += N[j] * R_In[i][j];
            sum_R_Out += (ones_N_g[j]) * R_Out[i][j];
        }
        JAC[i][i] = - (sum_R_In + sum_R_Out);
    }
    return JAC;
}

