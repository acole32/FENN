#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <iomanip>

std::vector<double> RestartCalculation(std::string FileDir, std::string FileName, int RestartFileNumber) {
    std::vector<double> N_Restart(40); 
    std::stringstream file;
    file << FileDir << "/" << FileName << "_" << std::setfill('0') << std::setw(8) << RestartFileNumber; 

    std::ifstream data(file.str());
    for (int i = 0; i < 40; i++) {
        double temp;
        data >> temp;
        N_Restart[i] = temp;
    }
    data.close();
    return N_Restart;
}
