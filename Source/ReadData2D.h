#include <vector>
#include <string>
#include <cmath>
#include <cstdio>

std::vector<std::vector<double> > ReadData2D(std::string FileName, int M, int N) {
    std::vector<std::vector<double> > Data2D(M,std::vector<double>(N));
    FILE* File = fopen(FileName.c_str(), "rb");
    double temp;
    if (File != NULL) {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                size_t elements_read = fread(&temp, sizeof(double), 1, File);
                if (elements_read != 1) {
                    fclose(File);
                    return std::vector<std::vector<double> >();
                }
                Data2D[j][i] = temp;
            }
        }
        fclose(File);
        return Data2D;
    }
    else {
        return std::vector<std::vector<double> >();
    }
}
