#include <vector>
#include <cmath>
#include <algorithm>
#include <numeric>

std::vector<double> RHS_FD(std::vector<double> N, std::vector<std::vector<double> > R_In, std::vector<std::vector<double> > R_Out, int N_g) {
    std::vector<double> RHS(N_g);
    for (int i = 0; i < N_g; i++) {
        double sum_R_In = 0.0, sum_R_Out = 0.0;
        for (int j = 0; j < N_g; j++) {
            sum_R_In += (1 - N[i]) * R_In[i][j];
            sum_R_Out += N[i] * (1 - N[j]) * R_Out[i][j];
        }
        RHS[i] = sum_R_In - sum_R_Out;
    }
    return RHS;
}
