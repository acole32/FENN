#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include "RHS_FD.h"
#include "JAC_FD.h"


std::pair<std::vector<double>, int> NewtonRaphson(std::vector<double> Nold, double dt, std::vector<std::vector<double> > R_In, std::vector<std::vector<double> > R_Out, int N_g, double Tol) {
    std::vector<double> Nnew = Nold;
    std::vector<std::vector<double> > JAC = JAC_FD(Nnew, R_In, R_Out, N_g);
    std::vector<double> dN(N_g, 0);
    int nIterations = 0;

    while(true) {
        std::vector<double> RHS = RHS_FD(Nnew, R_In, R_Out, N_g);

        for(int i = 0; i < N_g; i++) {
            double JAC_dN = 0;
            for(int j = 0; j < N_g; j++) {
                JAC_dN += JAC[i][j] * dN[j];
            }
            dN[i] = (Nold[i] - Nnew[i] + dt * RHS[i] - dt * JAC_dN) / (1 - dt * JAC[i][i]);
        }

        for (int i = 0; i < N_g; i++) {
            Nnew[i] += dN[i];
}

        nIterations++;

        double norm = 0;
        for(int i = 0; i < N_g; i++) {
            norm += std::abs(dN[i] / Nnew[i]);
        }
        norm = norm / N_g;

        if(norm < Tol) {
            break;
        }
    }

    return std::make_pair(Nnew, nIterations);
}
