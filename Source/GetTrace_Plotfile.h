#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>
#include <sstream>
#include <vector>
#include <cmath>
#include <gnuplot-iostream.h>
using namespace std; 

#define EB 40


void GetTrace_Plotfile(string FileDir, string FileName, int Lo, int Hi) { 

  int nFiles = Hi - Lo + 1; 

  vector<double> tVec(nFiles); 
  vector<double> dtVec(nFiles); 
  vector<double> dNVec(nFiles); 
  vector<vector<double>> kVec(nFiles, vector<double>(EB));   // 2D array of size nFiles x 40  
  vector<vector<vector<double>>> cMat(nFiles, vector<vector<double>> (EB, vector<double>(EB))); // 3D array of size nFiles x 40 x 40  
  vector<double> cycleVec(nFiles);  
  vector<double> truecycleVec(nFiles);  
  vector<double> nIterationsVec(nFiles);  
  vector<double> nTrueIterationsVec(nFiles);  
  vector<vector<double>> NMat (nFiles, vector<double>(EB)); // 2D array of size nfiles x 40   
  vector<vector < double >> eC (nFiles, vector < double > (EB)); // 2D array of size nfiles x 40
  vector<vector < double >> dv (nFiles, vector < double > (EB)); // 2D array of size nfiles x 40
  vector<double> Branch(nFiles);  
  vector<double> dt_EA(nFiles);  
  vector<double> dt_FE(nFiles);
  vector<double> dt_PE(nFiles);                    

    Gnuplot gp;

    gp << "set title 'Trace Plot'\n";

    gp << "set ylabel 'N'\n";

    gp << "set xlabel 'Time'\n";

    gp << "plot '-' with linespoints lw 2 pt 7 ps 0.5 lc rgb 'red' title 'N' \n";

     int iFile = 0;

     for (int i = Lo; i <= Hi; i++) {       

        iFile = iFile + 1;    

        std::stringstream ss;
        ss << FileDir << "/" << FileName << "_" << std::setfill('0') << std::setw(8) << i;
        std::string file_path = ss.str();

        std::ifstream t(file_path);
        std::string str((std::istreambuf_iterator<char>(t)),
                 std::istreambuf_iterator<char>());

        stringstream s(str);
        string temp;

        int k = 0;
        int z = 0;
        int y = 0;

        while( s >> temp){
            if ( k == 0){
                tVec[iFile] = temp;
                }
            if ( k == 1){
                dtVec[iFile] = temp;
                }
            if ( k > 1 && k < 43){
                NMat[iFile,k-2] = temp;
                }
            if ( k > 42 && k < 83){
                eC[iFile,k-42] = temp;
                }
            if ( k > 82 && k < 123){
                dv[iFile,k-82] = temp;
                }
            if ( k > 122 && k < 163){
                kVec[iFile,k-122] = temp;
                }
            if ( k > 162 ){
                
                if (z<(40*y){
                    cMat[iFile,y,z] = temp;
                }
                else {
                    y++;
                }
                

                z++;    
            }

            k++;





        }

        /*
        if (Data.HasField("t"))
            tVec[iFile] = Data["t"];
        else
            tVec[iFile] = 0;

        if (Data.HasField("dt"))
            tVec[iFile] = Data["dt"];
        else
            dtVec[iFile] = 0;

        if (Data.HasField("kVec"))
            kVec.row(iFile) = Data["kVec"];
        else
            kVec.row(iFile) = 0;

        if (Data.HasField("cMat"))
            cMat.slice(iFile) = Data["cMat"];
        else
            cMat.slice(iFile) = 0;

        if (Data.HasField("cycle"))
            cycleVec[iFile] = Data["cycle"];
        else
            cycleVec[iFile] = 0;

        if (Data.HasField("true_cycle"))
            truecycleVec[iFile] = Data["true_cycle"];
        else
            truecycleVec[iFile] = 0;

        if (Data.HasField("nIterations"))
            nIterationsVec[iFile] = Data["nIterations"];
        else
            nIterationsVec[iFile] = 0;

        if (Data.HasField("nTrueIterations"))
            nTrueIterationsVec[iFile] = Data["nTrueIterations"];
        else
            nTrueIterationsVec[iFile] = 0;

        if (Data.HasField("eC"))
            eC.row(iFile) = Data["eC"];
        else
            eC.row(iFile) = 0;

        if (Data.HasField("Branch"))
            Branch[iFile] = Data["Branch"];
        else
            Branch[iFile] = 0;

        if (Data.HasField("dt_FE"))
            dt_FE[iFile] = Data["dt_FE"];
        else
            dt_FE[iFile] = 0;

        if (Data.HasField("dt_EA"))
            dt_EA[iFile] = Data["dt_EA"];
        else
            dt_EA[iFile] = 0;

        if (Data.HasField("dt_PE"))
            dt_PE[iFile] = Data["dt_PE"];
        else
            dt_PE[iFile] = 0;

        if (Data.HasField("N")) {
            NMat.row(iFile) = Data["N"];
            dNVec[iFile] = (Data["N"] * Data["dV"].t()).sum();
        } else {
            NMat.row(iFile) = 0;
        } */

        for (int i = 1; i < dNVec.size(); i++)
            dNVec[i] = (dNVec[i] - dNVec[0]) / dNVec[0];
     }
