#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>
#include <sstream>
#include <vector>
#include <cmath>
//#include <gnuplot-iostream.h>
using namespace std; 

#define EB 40


 std::tuple<vector<double>, vector<double>, vector<vector<double>>, vector<vector<double>>, vector<vector < double >> >  GetTrace_Plotfile2(string FileDir, string FileName, int Lo, int Hi) { 
// vector<vector<vector<double>>>

  int nFiles = Hi - Lo + 1; 

  vector<double> tVec; 
  vector<double> dtVec; 
  //vector<double> dNVec; 
  //vector<vector<double>> kVec; 
  //vector<vector<vector<double>>> cMat; // 3D array of size nFiles x 40 x 40  
  //vector<double> cycleVec;  
  //vector<double> truecycleVec;  
  //vector<double> nIterationsVec;  
  //vector<double> nTrueIterationsVec;  
  vector<vector<double>> NMat;  
  vector<vector < double >> eC; // 2D array of size nfiles x 40
  vector<vector < double >> dv; // 2D array of size nfiles x 40
  //vector<double> Branch;  
  //vector<double> dt_EA;  
  //vector<double> dt_FE;
  //vector<double> dt_PE;    

int iFile = 0;

for (int i = Lo; i <= Hi; i++) {       

    iFile = iFile + 1;                 

        std::stringstream ss;
        ss << FileDir << "/" << FileName << i;
        std::string file_path = ss.str();
        std::ifstream input(file_path);

    // check that the file was opened successfully
    if (!input.is_open()) {
        std::cerr << "Failed to open file: " << FileName << '\n';
        break;
    }

    // read the data from the file
    double value;
    std::string line;
    while (std::getline(input, line)) {
        if (line == "t") {
            input >> value;
            tVec.push_back(value);
        } else if (line == "dt") {
            input >> value;
            dtVec.push_back(value);
        } else if (line == "N") {
            std::vector<double> N_temp;
            while (input >> value) {
                N_temp.push_back(value);
            }
            NMat.push_back(N_temp);

        } /*else if (line == "CMat") {
            std::vector<std::vector<double>> cMat_temp(40, std::vector<double>(40));
            while (input >> value) {
                cMat_temp.push_back(value);
            }
            cMat.push_back(cMat_temp);
        } /*else if (line == "CMat") {
            std::vector<std::vector<double>> cMat_temp(40, std::vector<double>(40));
            for (int i = 0; i < 40; i++) {
                for (int j = 0; j < 40; j++) {
                    input >> cMat_temp[i][j];
                }
            }
            cMat.push_back(cMat_temp);
        }*/
    }

    // check that we read in the data correctly
    std::cout << "t: ";
    for (const auto& elem : tVec) {
        std::cout << elem << ' ';
    }
    std::cout << '\n';

    std::cout << "dt: ";
    for (const auto& elem : dtVec) {
        std::cout << elem << ' ';
    }
    std::cout << '\n';

    std::cout << "N: ";
    for (const auto& row : NMat) {
        std::cout << '\n';
        for (const auto& elem : row) {
            std::cout << elem << ' ';
        }
    }
    std::cout << '\n';

   /* std::cout << "cMat: ";
    for (const auto& layer : cMat) {
        std::cout << '\n';
        for (const auto& row : layer) {
            for (const auto& elem : row) {
                std::cout << elem << ' ';
            }
            std::cout << '\n';
        }
    }
    std::cout << '\n'; */

    }
return std::make_tuple(tVec, dtVec, NMat, eC, dv);
}
