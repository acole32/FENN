#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdio.h>

#define EB 40

/*
int Write_Plotfile(double t, double dt, std::vector<double> N, std::vector<double> eC, std::vector<double> dV, std::vector<double> kVec, std::vector<std::vector<double> > cMat, int cycle, int true_cycle, int nIterations, int nTrueIterations, int Branch, double dt_FE, double dt_EA, double dt_PE, std::string FileDir, std::string BaseFileName, int FileNumber) {
    std::string FileName = FileDir + "/" + BaseFileName + "_" + std::to_string(FileNumber);

    std::ofstream file;
    file.open(FileName);
    file << t << " " << dt << " ";
    for (int i = 0; i < N.size(); i++) {
        file << N[i] << " ";
    }
    for (int i = 0; i < eC.size(); i++) {
        file << eC[i] << " ";
    }
    for (int i = 0; i < dV.size(); i++) {
        file << dV[i] << " ";
    }
    for (int i = 0; i < kVec.size(); i++) {
        file << kVec[i] << " ";
    }
    for (int i = 0; i < cMat.size(); i++) {
        for (int j = 0; j < cMat[i].size(); j++) {
            file << cMat[i][j] << " ";
        }
    }
    file << cycle << " " << true_cycle << " " << nIterations << " " << nTrueIterations << " " << Branch << " " << dt_FE << " " << dt_EA << " " << dt_PE;
    file.close();

    FileNumber++;
    return FileNumber;
}*/

/*

int Write_Plotfile2_Copy(double t, double dt, std::vector<double> N, std::vector<double> eC, std::vector<double> dV, std::vector<double> kVec, std::vector<std::vector<double> > cMat, int cycle, int true_cycle, int nIterations, int nTrueIterations, int Branch, double dt_FE, double dt_EA, double dt_PE, std::string FileDir, std::string BaseFileName, int FileNumber) {

    std::string FileName = FileDir + "/" + BaseFileName + "_" + std::to_string(FileNumber);
    FILE * fileID;
    fileID = fopen(FileName, "w");
    fprintf(fileID, "%20s %20s\r\n", "t", "dt");
    fprintf(fileID, "%25.25d %25.25d\r\n", t, dt);

    fprintf(fileID, "%12s\r\n","TC");
    fprintf(fileID, "%20s\r\n",true_cycle);
    //fprintf(fileID, "%20s %12s\r\n", "DN", "TC");
    //fprintf(fileID, "%25.25f %6.1f\r\n", DN, true_cycle);

    fprintf(fileID, "%6c\r\n", "N");
    for (int i = 0; i< EB; i++){
        fprintf(fileID, "% 12.8d\r\n", N(i));
    }

    fprintf(fileID, "%6s\r\n", "eC");
    for (int i = 0; i< EB; i++){
        fprintf(fileID, "% 12.8d\r\n", eC(i));
    }

    fprintf(fileID, "%6.4s\r\n", "CMat");
    for (int j = 0; j < EB; J++) {
        fprintf(fileID, "%12.8s %d\r\n", "// EB = ", j);
        for (int k = 0; k < EB; k++){
            fprintf(fileID, "% 12.8d\r\n", cMat(j, k));
        }
    }

    fclose(fileID);
    FileNumber++;
    return FileNumber;

    */

int Write_Plotfile2(double t, double dt, std::vector<double> N, std::vector<double> eC, std::vector<double> dV, std::vector<double> kVec, std::vector<std::vector<double> > cMat, int cycle, int true_cycle, int nIterations, int nTrueIterations, int Branch, double dt_FE, double dt_EA, double dt_PE, std::string FileDir, std::string BaseFileName, int FileNumber) {

    std::string FileName = FileDir + "/" + BaseFileName + "_" + std::to_string(FileNumber);
    FILE* fileID;
    fileID = fopen(FileName.c_str(), "w");
    fprintf(fileID, "t\n");
    fprintf(fileID, "%20.20f\n", t);
    fprintf(fileID, "dt\n");
    fprintf(fileID, "%20.20f\n", dt);


    fprintf(fileID, "TC\n");
    fprintf(fileID, "%d\n", true_cycle);

    fprintf(fileID, "N\n");
    for (int i = 0; i < EB; i++) {
        fprintf(fileID, "%12.8f\n", N[i]);
    }

    fprintf(fileID, "eC\n");
    for (int i = 0; i < EB; i++) {
        fprintf(fileID, "%12.8f\n", eC[i]);
    }

    fprintf(fileID, "CMat\n");
    for (int j = 0; j < EB; j++) {
        fprintf(fileID, "// EB = %d\n", j);
        for (int k = 0; k < EB; k++) {
            fprintf(fileID, "%12.8f\n", cMat[j][k]);
        }
    }

    fclose(fileID);
    FileNumber++;
    return FileNumber;
}