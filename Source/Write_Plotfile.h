int Write_Plotfile(double t, double dt, std::vector<double> N, std::vector<double> eC,
                   std::vector<double> dV, std::vector<double> kVec,
                   std::vector<std::vector<double> > cMat, int cycle, int true_cycle,
                   int nIterations, int nTrueIterations, int Branch, double dt_FE,
                   double dt_EA, double dt_PE, double tolC, double dt_grw, double dt_max,
                   std::string FileDir, std::string BaseFileName, int FileNumber)
{
    std::string FileName = FileDir + "/" + BaseFileName + "_" + std::to_string(FileNumber);

    std::ofstream file;
    file.open(FileName);

    file << "t: " << t << "\n";
    file << "dt: " << dt << "\n";

    file << "N: ";
    for (int i = 0; i < N.size(); i++) {
        file << N[i] << " ";
    }
    file << "\n";

    file << "eC: ";
    for (int i = 0; i < eC.size(); i++) {
        file << eC[i] << " ";
    }
    file << "\n";

    file << "dV: ";
    for (int i = 0; i < dV.size(); i++) {
        file << dV[i] << " ";
    }
    file << "\n";

    file << "kVec: ";
    for (int i = 0; i < kVec.size(); i++) {
        file << kVec[i] << " ";
    }
    file << "\n";

    file << "cMat: \n";
    for (int i = 0; i < cMat.size(); i++) {
        for (int j = 0; j < cMat[i].size(); j++) {
            file << cMat[i][j] << " ";
        }
        file << "\n";
    }

    file << "dt_FE: " << dt_FE << "\n";
    file << "dt_EA: " << dt_EA << "\n";
    file << "dt_PE: " << dt_PE << "\n";
    file << "tolC: " << tolC << "\n";
    file << "dt_grow: " << dt_grw << "\n";
    file << "dt_max: " << dt_max << "\n";
    file << "steps: " << cycle << "\n";

    file.close();

    FileNumber++;
    return FileNumber;
}