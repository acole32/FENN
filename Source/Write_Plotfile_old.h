#include <fstream>
#include <string>
#include <sstream>

int Write_Plotfile(double t, double dt, std::vector<double> N, std::vector<double> eC, std::vector<double> dV, std::vector<double> kVec, std::vector<std::vector<double> > cMat, int cycle, int true_cycle, int nIterations, int nTrueIterations, int Branch, double dt_FE, double dt_EA, double dt_PE, std::string FileDir, std::string BaseFileName, int FileNumber) {
    std::string FileName = FileDir + "/" + BaseFileName + "_" + std::to_string(FileNumber);

    std::ofstream file;
    file.open(FileName);
    file << t << " " << dt << " ";
    for (int i = 0; i < N.size(); i++) {
        file << N[i] << " ";
    }
    for (int i = 0; i < eC.size(); i++) {
        file << eC[i] << " ";
    }
    for (int i = 0; i < dV.size(); i++) {
        file << dV[i] << " ";
    }
    for (int i = 0; i < kVec.size(); i++) {
        file << kVec[i] << " ";
    }
    for (int i = 0; i < cMat.size(); i++) {
        for (int j = 0; j < cMat[i].size(); j++) {
            file << cMat[i][j] << " ";
        }
    }
    file << cycle << " " << true_cycle << " " << nIterations << " " << nTrueIterations << " " << Branch << " " << dt_FE << " " << dt_EA << " " << dt_PE;
    file.close();

    FileNumber++;
    return FileNumber;
}
