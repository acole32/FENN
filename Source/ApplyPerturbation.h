#include <random>
#include <algorithm>
#include <vector>


std::vector<double> ApplyPerturbation(std::vector<double> Nold, double amp, std::vector<double> dV, int N_g, int PertCase) {
    std::vector<double> N_Pert(N_g);
    if (amp == 0) {
        N_Pert = Nold;
    }
    else {
        if (PertCase == 1) {
            for (int i = 0; i < N_g; i++) {
                N_Pert[i] = (1.0 + amp * 2.0 * (0.5 - rand())) * Nold[i];
                N_Pert[i] = std::min(1.0, std::max(0.0, N_Pert[i]));
            }
        }
        else {
            if (PertCase == 2) {
                std::vector<double> N_prime(N_g);
                for (int i = 0; i < N_g; i++) {
                    N_prime[i] = (1.0 + amp * 2.0 * (0.5 - rand())) * Nold[i];
                    N_prime[i] = std::min(1.0, std::max(0.0, N_prime[i]));
                }
                double alpha = std::inner_product(Nold.begin(), Nold.end(), dV.begin(), 0.0) / std::inner_product(N_prime.begin(), N_prime.end(), dV.begin(), 0.0);
                for (int i = 0; i < N_g; i++) {
                    N_Pert[i] = alpha * N_prime[i];
                }
            }
            else {
                if (PertCase == 3) {
                    std::vector<double> N_prime(N_g);
                    for (int i = 0; i < N_g; i++) {
                        N_prime[i] = (1.0 + amp * 2.0 * (0.5 - rand())) * Nold[i];
                    }
                    // Implement linear least squares method here
                }
            }
        }
    }
    return N_Pert;
}
