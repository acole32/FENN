#include <vector>
#include <cmath>
#include <utility>

std::pair<std::vector<std::vector<double> >, std::vector<double> > BuildCollisionMatrix_NES(std::vector<std::vector<double> >& R_In, 
                              std::vector<std::vector<double> >& R_Out, 
                              std::vector<double>& N, 
                              std::vector<double>& dV, 
                              int N_g, 
                              double tol, 
                              std::vector<std::vector<double> >& A, 
                              std::vector<double>& k) {
    A.resize(N_g);
    for (int i = 0; i < N_g; i++) {
        A[i].resize(N_g, 0.0);
    }

    for (int i = 0; i < N_g; i++) {
        for (int j = 0; j < N_g; j++) {
        A[i][j] = 0;

        }
    } 

    k.resize(N_g);

    for (int i = 0; i < N_g; i++) {
        k[i] = 0;
    }  

    for (int i = 0; i < N_g; i++) {
        for (int j = 0; j < N_g; j++) {
            double N_Eq_i = 0;
            double N_Eq_j = 0;
            if(j != i) {
                double B = (R_In[i][j] * dV[i] + R_Out[i][j] * dV[j]);
                double C = dV[i] * N[i] + dV[j] * N[j];
                double a = (R_In[i][j] - R_Out[i][j]) * dV[i];
                double b = B + (R_In[i][j] - R_Out[i][j]) * C;
                double c = R_In[i][j] * C;
                double d = b*b - 4.0 * a * c;
                N_Eq_i = 0.5 * (b - sqrt(d)) / a;
                N_Eq_j = (C - (N_Eq_i * dV[i])) / dV[j];
            }
            else {
                N_Eq_i = N[i];
                N_Eq_j = N[j];
            }
                double diff_i = abs(N_Eq_i - N[i]) / std::max(1e-16, N_Eq_i);
                double diff_j = abs(N_Eq_j - N[j]) / std::max(1e-16, N_Eq_j);
            if (diff_i > tol || diff_j > tol) {
                A[i][j] += (1.0 - N[i]) * R_In[i][j] * dV[j];
                A[i][i] -= R_Out[i][j] * dV[j] * (1.0 - N[j]);
                k[i] += R_Out[i][j] * dV[j] + (R_In[i][j] * dV[j] - R_Out[i][j] * dV[j]) * N[j];
        }
    }
  }


  return std::make_pair(A, k);
}
