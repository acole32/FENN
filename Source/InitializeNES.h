#include <vector>
#include <string>
#include <cmath>
#include "ReadData1D.h"
#include "ReadData2D.h"

void InitializeNES(std::string Model, int N_g, std::vector<double>& eC, std::vector<double>& dV, std::vector<std::vector<double> >& R_In, std::vector<std::vector<double> >& R_Out, std::vector<double>& N_Eq) {
  // --- Energy Bin Centers ---

  eC = ReadData1D("Data/NES_RATES_EnergyBinCenter.dat", N_g);

  // --- Energy Bin Widths ---
 
  std::vector<double> de = ReadData1D("Data/NES_RATES_EnergyBinWidth.dat", N_g);

  // --- Energy Bin Volume ---

  dV.resize(N_g);
  for (int i = 0; i < N_g; i++) {
    dV[i] = ((eC[i] + 0.5*de[i])*(eC[i] + 0.5*de[i])*(eC[i] + 0.5*de[i]) - (eC[i] - 0.5*de[i])*(eC[i] - 0.5*de[i])*(eC[i] - 0.5*de[i]))/3.0;
  }

  // --- NES In-Scattering Rates ---

  R_In = ReadData2D("Data/NES_RATES_R_In___" + Model + ".dat", N_g, N_g);

  R_Out = R_In;
  for (int i = 0; i < N_g; i++) {
    for (int j = 0; j < N_g; j++) {
      R_Out[i][j] = R_In[j][i];
    }
  }

  if (Model == "001") {
    double mu = 145.254;
    double kT = 20.5399;
    N_Eq.resize(N_g);
        for (int i = 0; i < N_g; i++) {
          N_Eq[i] = 1.0/(exp((eC[i]-mu)/kT) + 1.0);
        }
    for (int j = 0; j < N_g; j++) {
      for (int i = 0; i < N_g; i++) {
        if (j < i) { // Impose Detailed Balance
          R_In[i][j] = R_In[j][i]*exp((eC[j]-eC[i])/kT);
        }
      }
    }
    for (int i = 0; i < N_g; i++) {
      for (int j = 0; j < N_g; j++) {
        R_Out[i][j] = R_In[j][i];
      }
    }
  } else if (Model == "002") {
    double mu = 45.835;
    double kT = 15.9751;
    N_Eq.resize(N_g);
        for (int i = 0; i < N_g; i++) {
          N_Eq[i] = 1.0/(exp((eC[i]-mu)/kT) + 1.0);
        }
    for (int j = 0; j < N_g; j++) {
      for (int i = 0; i < N_g; i++) {
        if (j < i) { // Impose Detailed Balance
          R_In[i][j] = R_In[j][i]*exp((eC[j]-eC[i])/kT);
        }
      }
    }
    for (int i = 0; i < N_g; i++) {
      for (int j = 0; j < N_g; j++) {
        R_Out[i][j] = R_In[j][i];
      }
    }
  } else if (Model == "003") {
    double mu = 20.183;
    double kT = 7.7141;
    N_Eq.resize(N_g);
        for (int i = 0; i < N_g; i++) {
          N_Eq[i] = 1.0/(exp((eC[i]-mu)/kT) + 1.0);
        }
    for (int j = 0; j < N_g; j++) {
      for (int i = 0; i < N_g; i++) {
        if (j < i) { // Impose Detailed Balance
          R_In[i][j] = R_In[j][i]*exp((eC[j]-eC[i])/kT);
        }
      }
    }
    for (int i = 0; i < N_g; i++) {
      for (int j = 0; j < N_g; j++) {
        R_Out[i][j] = R_In[j][i];
      }
    }
  } else if (Model == "004") {
    double mu = 9.118;
    double kT = 7.583;
    N_Eq.resize(N_g);
        for (int i = 0; i < N_g; i++) {
          N_Eq[i] = 1.0/(exp((eC[i]-mu)/kT) + 1.0);
        }
    for (int j = 0; j < N_g; j++) {
      for (int i = 0; i < N_g; i++) {
        if (j < i) { // Impose Detailed Balance
          R_In[i][j] = R_In[j][i]*exp((eC[j]-eC[i])/kT);
        }
      }
    }
    for (int i = 0; i < N_g; i++) {
      for (int j = 0; j < N_g; j++) {
        R_Out[i][j] = R_In[j][i];
      }
    }
  } else if (Model == "005") {
    double mu = 3.886;
    double kT = 3.1448;
    N_Eq.resize(N_g);
        for (int i = 0; i < N_g; i++) {
          N_Eq[i] = 1.0/(exp((eC[i]-mu)/kT) + 1.0);
        }
    for (int j = 0; j < N_g; j++) {
      for (int i = 0; i < N_g; i++) {
        if (j < i) { // Impose Detailed Balance
          R_In[i][j] = R_In[j][i]*exp((eC[j]-eC[i])/kT);
        }
      }
    }
    for (int i = 0; i < N_g; i++) {
      for (int j = 0; j < N_g; j++) {
        R_Out[i][j] = R_In[j][i];
      }
    }
  }
}
