#include <cmath>
#include <vector>

std::vector<double> logspace(double start, double end, int n) {
    std::vector<double> result;
    for (int i = 0; i < n; i++) {
        double log_val = start + (end - start) * i / (n - 1);
        result.push_back(std::pow(10, log_val));
    }
    return result;
}
