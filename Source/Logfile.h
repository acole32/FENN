#include <iostream>
#include <fstream>
using namespace std;

void Logfile(double t_end, double t, double t_W0, double dt, double G_A, double G_B, double G_C, double tolC, string Scheme, string Comment)
{
    string savePath = "./Run/Output_Log";
    ofstream fileID;
    fileID.open("log.txt",ios::app);
    fileID<<" t_end ="<<t_end<<endl;
    fileID<<" t ="<<t<<endl;
    fileID<<" t_W0 ="<<t_W0<<endl;
    fileID<<" dt ="<<dt<<endl;
    fileID<<" G_A ="<<G_A<<endl;
    fileID<<" G_B ="<<G_B<<endl;
    fileID<<" G_C ="<<G_C<<endl;
    fileID<<" tolC ="<<tolC<<endl;
    fileID<<" Scheme="<<Scheme<<endl;
    fileID<<" Comment: "<<Comment<<endl;
    fileID<<endl;
    fileID.close();
}
