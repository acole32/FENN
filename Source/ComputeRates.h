#include <vector>
#include <utility>

std::pair<std::vector<double>, std::vector<double> > ComputeRates(const std::vector<std::vector<double> > &R_In, 
                                                                  const std::vector<std::vector<double> > &R_Out, 
                                                                  const std::vector<double> &N, 
                                                                  const std::vector<double> &dV)
{
    int N_g = N.size();
    std::vector<double> F(N_g);
    std::vector<double> k(N_g);
    for (int i = 0; i < N_g; i++)
    {
        F[i] = 0;
        k[i] = 0;
        for (int j = 0; j < N_g; j++)
        {
            F[i] += R_In[i][j] * N[j] * dV[j];
            k[i] += R_In[i][j] * N[j] * dV[j] + R_Out[i][j] * (1.0 - N[j]) * dV[j];
        }
    }
    return std::make_pair(F, k);
}
