#include <vector>
#include <string>
#include <cmath>
#include <cstdio>

std::vector<double> ReadData1D(std::string FileName, int N) {
    std::vector<double> Data1D;
    FILE* File = fopen(FileName.c_str(), "rb");
    double temp;
    if (File != NULL) {
        for (int i = 0; i < N; i++) {
            size_t elements_read = fread(&temp, sizeof(double), 1, File);
            if (elements_read != 1) {
                fclose(File);
                return std::vector<double>();
            }
            Data1D.push_back(temp);
        }
        fclose(File);
        return Data1D;
    }
    else {
        return std::vector<double>();
    }
}
